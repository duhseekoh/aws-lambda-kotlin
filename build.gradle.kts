import org.jetbrains.kotlin.gradle.dsl.Coroutines
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.2.51"
}

repositories {
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib", "1.2.51"))

    implementation("com.amazonaws:aws-lambda-java-core:1.1.0")
    implementation("com.amazonaws:aws-lambda-java-events:2.1.0")
    implementation("com.amazonaws:aws-lambda-java-log4j2:1.0.0")
    implementation("com.fasterxml.jackson.core:jackson-core:2.6.7")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.6.7.1")
    implementation("io.github.microutils:kotlin-logging:1.5.4")
    implementation("org.apache.logging.log4j:log4j-api:2.8.2")
    implementation("org.apache.logging.log4j:log4j-core:2.8.2")
    implementation("org.apache.logging.log4j:log4j-slf4j-impl:2.8.2")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:0.22.5")
}

kotlin {
    experimental {
        coroutines = Coroutines.ENABLE
    }

}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
        javaParameters = true
    }
}


tasks {
    get("compileKotlin").dependsOn(get("processResources"))

    val buildZip by creating(Zip::class) {
        from(get("compileKotlin"))
        from(get("processResources"))

        version = ""

        into("lib") {
            from(configurations.runtimeClasspath)
        }
    }

    val build by getting {
        dependsOn(buildZip)
    }
}
