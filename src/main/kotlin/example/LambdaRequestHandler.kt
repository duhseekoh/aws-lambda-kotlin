package example

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent
import mu.KLogging
import mu.withLoggingContext
import java.time.Duration
import java.time.temporal.ChronoUnit

class LambdaRequestHandler {
    companion object : KLogging()

    suspend operator fun invoke(
        request: APIGatewayProxyRequestEvent,
        context: Context
    ): APIGatewayProxyResponseEvent {
        val startTimeNs = System.nanoTime()

        val response = APIGatewayProxyResponseEvent().apply {
            statusCode = 200
        }

        val durationMs = Duration.of(System.nanoTime() - startTimeNs, ChronoUnit.NANOS).toMillis()

        withLoggingContext(
            "request.endpoint" to "${request.httpMethod} ${request.resource}",
            "response.statusCode" to response.statusCode.toString(),
            "durationMs" to durationMs.toString(),
            "request" to request.toString(),
            "response" to response.toString()
        ) {
            logger.info { "requestCompleted" }
        }

        return response
    }
}