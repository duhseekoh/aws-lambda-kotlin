package example

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent
import example.config.Components
import kotlinx.coroutines.experimental.runBlocking

class Main : Components(),
    RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private val requestHandler = LambdaRequestHandler()

    override fun handleRequest(input: APIGatewayProxyRequestEvent, context: Context) =
        runBlocking {
            requestHandler(input, context)
        }
}