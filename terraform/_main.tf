provider "aws" {
  version = "~> 1.10"
  region  = "${var.aws_region}"
}

provider "template" {
  version = "~> 1.0"
}

locals {
  name           = "aws-lambda-kotlin-demo"
  lambda_archive = "../build/distributions/aws-lambda-kotlin.zip"
  lambda_name    = "${local.name}-service"
}
