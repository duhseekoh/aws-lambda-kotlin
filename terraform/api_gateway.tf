data "template_file" "api_swagger" {
  template = "${file("templates/swagger.yaml")}"

  vars {
    api_name       = "${local.name}"
    aws_lambda_arn = "${aws_lambda_function.lambda.arn}"
    aws_region     = "${var.aws_region}"
    iam_role_arn   = "${aws_iam_role.integration_role.arn}"
  }
}

resource "aws_api_gateway_rest_api" "api" {
  name = "${local.name}"
  body = "${data.template_file.api_swagger.rendered}"
}

resource "aws_iam_role" "integration_role" {
  name = "${local.name}-api-integration-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "integration_role_invoke_lambda" {
  role       = "${aws_iam_role.integration_role.name}"
  policy_arn = "${aws_iam_policy.invoke_lambda.arn}"
}

resource "aws_api_gateway_deployment" "api_deployment" {
  rest_api_id = "${aws_api_gateway_rest_api.api.id}"
  stage_name  = "deployment"

  # Workaround to ensure the gateway gets redeployed whenever the API changes.
  # This workaround would likely be too blunt an instrument if you are using
  # multiple stages in one API.
  stage_description = "[api_hash = ${md5(data.template_file.api_swagger.rendered)}]"

  lifecycle {
    create_before_destroy = true
  }
}
