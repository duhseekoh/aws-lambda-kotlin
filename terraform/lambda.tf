resource "aws_lambda_function" "lambda" {
  function_name    = "${local.lambda_name}"
  runtime          = "java8"
  filename         = "${local.lambda_archive}"
  source_code_hash = "${base64sha256(file(local.lambda_archive))}"
  memory_size      = 512
  timeout          = 30

  handler = "example.Main::handleRequest"
  role    = "${aws_iam_role.role.arn}"

  //  environment {
  //    variables = {}
  //  }
}

resource "aws_iam_role" "role" {
  name = "${local.lambda_name}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_basic_execution_role" {
  role       = "${aws_iam_role.role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_policy" "invoke_lambda" {
  name = "${local.name}-service-invoke"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Sid": "",
          "Effect": "Allow",
          "Action": "lambda:InvokeFunction",
          "Resource": "${aws_lambda_function.lambda.arn}"
      }
  ]
}
EOF
}
